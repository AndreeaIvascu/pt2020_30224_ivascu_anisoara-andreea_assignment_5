package assgn5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Activities {

	private static String InputFile = "Activity.txt";
	private String dataStart;
	private String dataEnd;
	private String activity;
	private ArrayList<MonitoredData> list = new ArrayList<MonitoredData>();
	private Map<String, Integer> nrActivities = new HashMap<String, Integer>();
	private Map<Integer, Map<String, Long>> activityCount = new HashMap<Integer, Map<String, Long>>();
	private Map<String, LocalTime> localTime = new HashMap<String, LocalTime>();
	
  //TASK1--create list of objects of type MonitoredData
	public List<MonitoredData> citire() {
		try {
			return Files.lines(Paths.get(InputFile)).map(s -> s.split("\t\t")).map(s -> {
				dataStart = s[0].replace(" ", "T");
				dataEnd = s[1].replace(" ", "T");
				activity = s[2];

				MonitoredData data = new MonitoredData();
				data.setActivity(activity);
				data.setStart(LocalDateTime.parse(dataStart));
				data.setEnd(LocalDateTime.parse(dataEnd));
				return data;
			}).collect(Collectors.toList());
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
   
	//TASK2--count the distinct days that appear in the monitoring data
	public int nrOfDays(List<MonitoredData> d) {
		List<MonitoredData> date = new ArrayList<MonitoredData>();
		date = d;
		int nr = 0;
		nr = date.stream().map(MonitoredData::aboutData).collect(Collectors.toSet()).size();
		return nr;
	}
    
	//TASK3--count how many times each activity has appeared over the entire monitoring period
	public Map<String, Long> nrOfActivities(List<MonitoredData> d) {
		List<MonitoredData> date = new ArrayList<MonitoredData>();
		date = d;
		Map<String, Long> mapActivity = new HashMap<String, Long>();
		mapActivity = date.stream().collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
		return mapActivity;

	}
    
	//TASK4--count for how many times each activity has appeared for each day over the monitoring period
	public Map<Integer, Map<String, Long>> nrActivitiesPerDay(List<MonitoredData> d) {
		Map<Integer, Map<String, Long>> activityCount = new HashMap<Integer, Map<String, Long>>();
		activityCount = d.stream().collect(Collectors.groupingBy(date -> date.dayStart(),
				Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));
		return activityCount;
	}
    
	
	//TASK5--compute the entire duration for each activity over the monitoring period
	public Map<String, Integer> activityTime(List<MonitoredData> d) {

		Map<String, Integer> activityTime = new HashMap<String, Integer>();
		activityTime = d.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.collectingAndThen(
						Collectors.mapping(date -> date.getTime().toMinutes(), Collectors.summingLong(Long::intValue)),
						Long::intValue)));
		return activityTime;
	}

	

}
