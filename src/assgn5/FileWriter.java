package assgn5;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileWriter {

	private String OutputFile1 = "ActivityOutTask1.txt";
	private String OutputFile2 = "ActivityOutTask2.txt";
	private String OutputFile3 = "ActivityOutTask3.txt";
	private String OutputFile4 = "ActivityOutTask4.txt";
	private String OutputFile5 = "ActivityOutTask5.txt";

	public void writer() {

		try {
			PrintWriter output1 = new PrintWriter(OutputFile1);
			PrintWriter output2 = new PrintWriter(OutputFile2);
			PrintWriter output3 = new PrintWriter(OutputFile3);
			PrintWriter output4 = new PrintWriter(OutputFile4);
			PrintWriter output5 = new PrintWriter(OutputFile5);

			Activities activ = new Activities();
			List<MonitoredData> date = new ArrayList<MonitoredData>();
			date = activ.citire();
			List<MonitoredData> d = new ArrayList<MonitoredData>();
			Map<String, Long> nrActivities = new HashMap<String, Long>();
			Map<Integer, Map<String, Long>> activityCount = new HashMap<Integer, Map<String, Long>>();
			Map<String, Integer> localTime = new HashMap<String, Integer>();
			d = activ.citire();

			localTime = activ.activityTime(d);
			Map<String, Long> nrActiv = activ.nrOfActivities(d);
			Map<Integer, Map<String, Long>> activityPerDay = activ.nrActivitiesPerDay(d);
			List<String> numeActivitati = new ArrayList<String>();

			int nrZile = activ.nrOfDays(d);

			output1.println("TASK1: " + "activitatile din fisier");
			for (int i = 0; i < date.size(); i++)
				output1.println(
						i + ":" + " startTimeofActivity: " + date.get(i).getStart() + "          endTimeofActivity: "
								+ date.get(i).getEnd() + "        nameOfActivity: " + date.get(i).getActivity());

			output2.println("TASK2: " + "numar de zile distincte ");
			output2.println(nrZile);

			output3.println("TASK3: " + "numar de activitati distincte");
			output3.println(nrActiv);

			output4.println("TASK4: " + "numar de activitati pe zi");
			output4.println(activityPerDay);

			output5.println("TASK5: " + "durata exprimata in minute pentru fiecare activitate ");
			output5.println(localTime);

			output1.close();
			output2.close();
			output3.close();
			output4.close();
			output5.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
}
