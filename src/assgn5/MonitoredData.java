package assgn5;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Date;

public class MonitoredData {
	private LocalDateTime start;
	private LocalDateTime end;
	private String activity;
	
	
	public MonitoredData(LocalDateTime start, LocalDateTime end, String activity) {
		super();
		this.start = start;
		this.end = end;
		this.activity = activity;
	}
	
	public MonitoredData()
	{
		
	}
	public LocalDateTime getStart() {
		return start;
	}
	public void setStart(LocalDateTime start) {
		this.start = start;
	}
	public LocalDateTime getEnd() {
		return end;
	}
	public void setEnd(LocalDateTime end) {
		this.end = end;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	  public String aboutData() {
	      String s="";
	      String del="-";
	      String an=""+start.getYear();
	      String luna=""+start.getMonthValue();
	      String zi=""+start.getDayOfMonth();
	      s=s+an+del+luna+del+zi;
		  return s;
	    }
	  
	  public int dayStart()
	  {
		  return start.getDayOfMonth();
	  }
	
	  
	  public int monthStart()
	  {
		  return start.getMonthValue();
	  }
	  
	  public int yearStart()
	  {
		  return start.getYear();
	  }
	  public int dayEnd()
	  {
		  return end.getDayOfMonth();
	  }
	
	  
	  public int monthEnd()
	  {
		  return end.getMonthValue();
	  }
	  
	  public int yearEnd()
	  {
		  return end.getYear();
	  }
	  
	  //Metoda1 pentru task-ul 5 -> durata unei activitati este de tipul integer => exprim durata in minute
	  public Duration getTime() {
	        
	        Duration timeOfActivity=Duration.between(start, end);
	        return timeOfActivity;
	        
	    }
	  
     //Metoda2 pentru task-ul 5 -> durata unei activitati este de tipul LocalTime
	public LocalTime getTime2 () {
		
		  int hour=start.getHour()-end.getHour();
		  int minute=start.getMinute()-end.getMinute();
		  int sec=start.getSecond()-end.getSecond();
		 
		  LocalTime specificTime = LocalTime.of(hour,minute,sec);
	
	
		   return specificTime;
		  
	  }
	  
	
	
	
	
	

}
